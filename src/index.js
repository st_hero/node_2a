import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());

function add(a, b) {
  return (a || 0) + (b || 0);
}

app.get('/2A', (req, res) => {
  console.log(req.query);
  const a = +req.query.a;
  const b = +req.query.b;

  res.send(add(a,b).toString());
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
